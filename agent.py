import cogment
import cog_settings

import data_pb2
import asyncio

async def my_agent(actor, trial):
    print("agent is starting...")
    observation = await actor.start()

    print("Initial observation received")
    while not trial.over:
        print(f"{actor.name} has observed {observation}")
        observation = await actor.do_action(data_pb2.Action(value=12))      

    print(f"{actor.name}'s trial is over...")


async def main():
    print("agent is alive")

    server = cogment.Server(cog_project=cog_settings, port=9000)

    server.register_actor(
        impl=my_agent, 
        impl_name="default", 
        actor_class="player")

    await server.run()

asyncio.run(main())

import cogment
import cog_settings

import data_pb2
import asyncio


async def my_environment(env, trial):
    obs =  data_pb2.Observation(value=-1)
    observations = [("*", obs)]

    print("starting...")
    env.start(observations)
    print("started...")
    
    for i in range(10):
        print("gathering...")
        # Wait until the next set of actions is available
        actions = await env.gather_actions()

        print("gathered")

        # Produce the next set of observations
        obs = data_pb2.Observation(value=i)
        env.produce_observations([("*", obs)])

        print("produced")

    obs = data_pb2.Observation(value=100)
    env.end(final_observations=[("*", obs)])

    print( "trial is done, env::End equivalent")

async def main():
    print("bringing up the env")
    server = cogment.Server(cog_project=cog_settings, port=9000)

    server.register_environment(impl=my_environment)

    await server.run()

asyncio.run(main())

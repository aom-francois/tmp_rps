FROM py_env

WORKDIR /app

ADD agent.py .
ADD cog_settings.py .
ADD *_pb2.py .

CMD ["python", "-u", "agent.py"]

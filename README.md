# instructions

navigate to cogment-orchestrator (on the py_sd_support branch)
```
docker build -t orch .
```

navigate to cogment-py_sdk (on the 1.0-dev branch)
```
docker build -t py_env .
```

navigate to rps
```
docker-compose build
cogment run start
```

in another window
```
cogment run play
```
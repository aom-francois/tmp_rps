
FROM orch

ADD cogment.yaml .
ADD *.proto .

ENTRYPOINT ["catchsegv"]
CMD ["orchestrator_dbg", "--config=/app/cogment.yaml"]

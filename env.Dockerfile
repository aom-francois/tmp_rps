FROM py_env

ENV PYTHONPATH /app

WORKDIR /app

ADD env.py .
ADD cog_settings.py .
ADD *_pb2.py .

CMD ["python", "-u", "env.py"]

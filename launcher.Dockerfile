FROM py_env

ENV PYTHONPATH /app

WORKDIR /app

ADD launcher.py .
ADD cog_settings.py .
ADD *_pb2.py .

CMD ["python", "-u", "launcher.py"]

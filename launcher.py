import cogment
import cog_settings

import data_pb2
import asyncio

async def my_agent(actor, trial):
    print("starting...")
    observation = await actor.start()
    print("started...")

    while not trial.over:
        print(f"{actor.name} has observed {observation}")
        observation = await actor.do_action(data_pb2.Action(value=12))      

async def main():
  print("aasdasd")
  connection = cogment.Connection(
      cog_project=cog_settings, endpoint="orchestrator:9000")

  print("Connected!")
  # Create a new trial
  trial = await connection.start_trial(
      data_pb2.TrialConfig(), user_id="This_is_a_test")

  print("started!")
  await connection.join_trial(trial_id=trial.id_, actor_id=0, actor_class="player", impl=my_agent)


asyncio.run(main())
